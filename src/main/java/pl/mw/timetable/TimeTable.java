package pl.mw.timetable;

import edu.princeton.cs.algs4.*;

/**
 * Created by mwiesiolek on 12.03.2017.
 */
public class TimeTable {

    public static void main(String[] args) {
        In in = new In(args[0]);
        FlowNetwork G = new FlowNetwork(in);
        int s = 0, t = G.V() - 1;
        StdOut.println(G);

        EdgeWeightedDigraph digraph = new EdgeWeightedDigraph(G.V());

        // compute maximum flow and minimum cut
        FordFulkerson maxflow = new FordFulkerson(G, s, t);
        StdOut.println("Max flow from " + s + " to " + t);
        for (int v = 0; v < G.V(); v++) {
            for (FlowEdge e : G.adj(v)) {
                if ((v == e.from()) && e.flow() > 0) {
                    StdOut.println("   " + e);
                    digraph.addEdge(new DirectedEdge(e.from(), e.to(), e.flow()));
                }
            }
        }

        // print min-cut
        StdOut.print("Min cut: ");
        for (int v = 0; v < G.V(); v++) {
            if (maxflow.inCut(v)) StdOut.print(v + " ");
        }
        StdOut.println();

        StdOut.println("Max flow value = " + maxflow.value());

        dfs(digraph, 0);
    }

    private static void dfs(EdgeWeightedDigraph G, int v) {
        for (DirectedEdge edge : G.adj(v)) {
            System.out.print(edge.from() + "->" + edge.to() + " ");
            if (edge.weight() > 0) {
                edge.decreaseWeightBy(1);

                dfs(G, edge.to());
                System.out.println();
            }
        }
    }
}
